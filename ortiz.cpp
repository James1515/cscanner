// ******************************************
// * 
// * Author: James Anthony Ortiz
// * COP4020 - Programming Languages 
// * proj1 (Scanner for C Language)
// * Compile: g++ -g -o cscan ortiz.cpp
// *
// ******************************************

//Pre-processor Library
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string>
#include <vector>
#include <iomanip>

using namespace std;

//Struct for Tokenizer
struct Tokenizer
{

  Tokenizer()//constructor
  {
    tokCount = 1;
    ident_ = false;
    string_ = false;
    num_ = false;
    chrlit_ = false;
  }

  int tokCount;//Number of token occurences
  string contents;//Tokenizer physical representation
  bool ident_, string_, num_, chrlit_;//Bool identifying type of
                                                  //token
};//Tokenizer Data Structure

//Variables
string tokenSegment;//Word being worked on
int line = 1;//Line number

//Vectors and Iterators
vector<Tokenizer> Tokens;//Vector containing tokens
vector<Tokenizer>::iterator itr;//Iterator for a  vector

//Prototypes
void Parse();//Iterates through input, determines type
void Print();//Prints all captured tokens and counts
void AddtoVector(Tokenizer t);//Adds Tokenizer to vector or increments token tokCount
bool ifOneChToken_(char c);//Checks if One character token
bool ifTwoChToken_(char c, char c2);//Checks if Two character token
bool ifTOrThreeChToken_(char c);//Checks if token is possible three or two
                                   //char tokens
bool isNumber(char c);//Checks if token is a number
bool isIdentifier(char c);//Checks if token is identifier
bool isCharLiteral(char c);//Checks if token is character literal
bool isString(char c);//Checks if token is string
void SortVector();//Sorts vector by most encountered token and length of
                  //contents
void IgnoreComment(char c);//Checks for comment and ignores it


int main()//Main
{
  Parse();//Call parser
  Print();//Print results of parser
}//END Main 

void Parse()//Picks up tokens one by one, checks type, adds to vector
{
  string word;//Contents of current token
  char c;//Current character

  while(!cin.eof())//While we have not reached the end of the file
  {
    tokenSegment = "\0";
    Tokenizer* t = new Tokenizer();//Creates token object we will populate

    cin.get(c);//Acquire next character from stream


    if(c == '\n')//Encountered newline, 
    {
      line++;//Increment line tokCount
      //cout << "Newline encountered. Now on line: " << line << endl;
      continue;//Do not add to vector, continue to next loop iteration
    }
    else if(isspace(c))//Encountered whitespace
    {
      continue;//Do not add to vector, continue to next character
    }
    else if(ifTwoChToken_(c,cin.peek()))//Check if char aquired is part of two
                                         //character token
    {
      word = c;//Add both characters to word
      cin.get(c);
      word.push_back(c);
      //cout << word << " is two character token.\n";
}
else if(ifTOrThreeChToken_(c))//Check if char aquired is part of two or three character token
{
word = tokenSegment;//Set word we grabbed to word gathered from function
//cout << word << " is two or three character token.\n";
}
else if(ifOneChToken_(c))//Check if char is standalone token
{
word = c;//Set word to grabbed token
//cout << word << " is one character token.\n";
}
else if(isNumber(c))//Check if char is number
{
word = tokenSegment;//Set word to number token grabbed from function
t->num_ = true;//Set boolean to identify type of token is number
//cout << word << " is Number.\n";
}
else if(isIdentifier(c))//Check if char is part of identifier
{
word = tokenSegment;//Set word to token grabbed from function
t->ident_ = true;//Set boolean to identify token is identifier
//cout << word << " is identifier.\n";
}
else if(isString(c))//Check if char is Ditto
{
word = tokenSegment;//Set word to token grabbed from function
t->string_ = true;//Set boolean to identify token as string
//cout << word << " is string.\n";
}
else if(isCharLiteral(c))//Check if char is '
{
word = tokenSegment;//Set word to token grabbed from function
t->chrlit_ = true;//Set bool to identify token as character literal
//cout << word << " is Character literal.\n";
}
else//Ignore cases
{
if(c == '/')//Possible comment
{
//cout << "Got a comment.\n";
IgnoreComment(c);//Check if comment and ignore
continue;//Do not add to vector, continue loop
}
else if(isspace(tokenSegment[0]))
continue;
else//Character added does not fit any types, so ignore
{
cerr << "illegal character: " << c  << " on line " << line << endl;//Print out error
continue;//Do not add to vector, grab next character
}
}

t->contents = word;//Set contents of token to grabbed characters

AddtoVector(*t);//Add populated token to vector

}
}

void AddtoVector(Tokenizer t)//Takes token and adds to vector or increments that tokens tokCount
{
cout << t.contents << endl;
for(itr = Tokens.begin(); itr != Tokens.end(); itr++)//Loop through tokens
{

//cout << "Compared word: " << itr->contents << " \nNew word: " << t.contents << endl;
if(t.contents == itr->contents)//Match found
{
if(itr->ident_ || itr->num_ || itr->chrlit_ || itr->string_)//Is not a special type of token
break;//Do not increment tokCount

itr->tokCount++;//Increase tokCount of token encounters
//cout << "Word " << itr->contents << " repeated" << endl;
//cout << t.contents << endl;//Print character
return;//Do not add to vector
}
}

//cout << "Word add successful: " << t.contents << endl;
Tokens.push_back(t);//Add new token to vector

}

void Print()//Print all gathered tokens and tokCount of tokens
{
Tokenizer t1,t2,t3,t4;//Create tokens for the different types of tokens: Number, string, Identifier, Character literal
t1.contents = "number";
t2.contents = "string";
t3.contents = "ident";
t4.contents = "char";

t1.tokCount = t2.tokCount = t3.tokCount = t4.tokCount = 0;//Set counts for these different types of tokens to 0

for(itr = Tokens.begin(); itr != Tokens.end(); itr++)//Print out all gathered
                                                     //tokens, and increment
                                                     //number of types
 {
   //cout << itr->contents << endl;//Print out token content

   if(itr->num_)//Found type of token, increment its type 
   {
     t1.tokCount++;
   }
   else if(itr->ident_)
   {
     t3.tokCount++;
   }
   else if(itr->chrlit_)
   {
     t4.tokCount++;
   }
   else if(itr->string_)
   {
     t2.tokCount++;
   }

   if(itr->chrlit_ || itr->string_ || itr-> ident_ || itr->num_)
   {
     Tokens.erase(itr);//Erase individual token, vector will auto adjust
     itr--;//Back up iterator to get new token
   }
 }

 if(t4.tokCount != 0)//Type of token is present, add it to vector
   Tokens.insert(Tokens.begin(),t4);
 if(t3.tokCount != 0)
   Tokens.insert(Tokens.begin(),t3);
 if(t2.tokCount != 0)
   Tokens.insert(Tokens.begin(),t2);
 if(t1.tokCount != 0)
   Tokens.insert(Tokens.begin(),t1);



 cout << "\n" << setw(13) << "token" << setw(16) << "Count\n";//Print header
 cout  << "---------------------" << setw(8) << "-----\n";

 
 SortVector();//Sort vector to descending order of token tokCount 
 
 for(itr = Tokens.begin(); itr != Tokens.end(); itr++)//Print out tokens in
                                                      //sorted order
 {
   cout << setw(21) << itr->contents  << setw(7) << itr->tokCount << endl;
 }

}

bool ifOneChToken_(char c)//See if grabbed character is one of the one
                           //character tokens
{
  switch(c)//Compare grabbed token to list of accepted one char tokens
  {
    case '(':
    case ')':
    case '*':
    case '+':
    case '-':
    case '=':
    case ',':
    case '.':
    case '!':
    case ':':
    case ';':
    case '?':
    case '[':
    case ']':
    case '{':
    case '}':
    case '~':
    case '<':
    case '>':
    case '&':
    case '%':
    case '|':
      return true;
    default:
      return false;
  }
}

bool ifTwoChToken_(char c, char c2)//See if grabbed character and peeked character are part of a character token
{
  if(c == '&' && c2 == '&')//Compare two to accepted two character tokens
  {
    return true;
  }
  else if(c == '|' && c2 == '|')
  {
    return true;
  }
  else if(c == '+' && c2 == '+')
  {
    return true;
  }
  else if(c == '-' && c2 == '-')
  {
    return true;
  }
  else if(c == '-' && c2 == '>')
  {
    return true;
  }
  else 
  {
    return false;
  }

}

bool ifTOrThreeChToken_(char c)//See if character is a part of optional two
                                  //or three character token
{
  switch(c)//See if first token is part of an accepted two character token
  {
    case '|':
    case '^':
    case '&':
    case '+':
    case '-':
    case '%':
    case '*':
    case '/':
    case '=':
    case '!'://Check rest
      {
        if(cin.peek() == '=')
        {
          tokenSegment = c;
          cin.get(c);
          tokenSegment.push_back(c);
          return true;
        }

        break;
      }
    case '>'://See if arrow is followed by other arrow
    case '<':
      {
        tokenSegment = c;

        if(cin.peek() == c)
        {
          cin.get(c);
          tokenSegment.push_back(c);

          if(cin.peek() == '=')//Check for optional = character
          {
            cin.get(c);
            tokenSegment.push_back(c);
          }

          return true;
        }
        else if(cin.peek() == '=')//Check if just <= or >=
        {
          cin.get(c);
          tokenSegment.push_back(c);

          return true;
        }

      }

  }

  return false;
}

bool isNumber(char c)//See if character is number, pick up all subsequent number
                     //characters till whitespace
{
  if(isdigit(c))//Check first character
  {
    tokenSegment = c;//We have a number, add it to the token we're making
    c = cin.peek();//Store next character but do not grab it
    while(isdigit(c) && c != cin.eof())//While we keep getting numbers
    {
      cin.get(c);//Grab the next character
      tokenSegment.push_back(c);//Add it
      c = cin.peek();//See what the next character is
    }
    return true;
  }
  else 
    return false;
}

bool isIdentifier(char c)//See if character is start of an identifier
{
  
  if(isalpha(c) || c == '_')//We have a character or _, we need the rest of the
                            //identifier
  {
    tokenSegment = c;//Add c to token we are making
    c = cin.peek();//Store next character but don't grab it
    while((isalnum(c) | c == '_') && c != cin.eof())//While we keep getting
                                                    //alphanumeric characters
                                                    //(or whitespaces)
    {
      cin.get(c);//Pick up the next character
      tokenSegment.push_back(c);//Add it to the word
      c = cin.peek();//See what the next characte
    }
    return true;
  }
  else
    return false;
  
}

bool isCharLiteral(char c)//Determines if character is part of character literal
{
  if(c == '\'')//See if character passed in is openning quote
{
tokenSegment = c;//Add character to word we're producing

 if(cin.peek() == '\'')//Check for empty literal
 {
   cin.get(c);//Get next character
   cerr <<"character has zero length on line " << line << endl;//Print error
                                                               //message
   tokenSegment = " ";
   return false;
 }

 do//Take everything until next quote
 {
   cin.get(c);//Get next character
   tokenSegment.push_back(c);//Add character to word
   //cout << "c is: " << c << " Peek is: " << (char)cin.peek() << endl;

   if(cin.peek() == '\n')//Ensure that character literal closes before new line
   {
     cin.get(c);//Get newline
     cerr << "missing ' for " << tokenSegment << " on line " << line << endl;//Print
                                                                           //error
                                                                           //message
     line++;//Increment line number
     tokenSegment = " ";//Reset global token contents
     return false;//Indicate false token
   }
   if(cin.peek() == '\\' && c == '\\')//Encountering \\ in char literal 
   {
     cin.get(c);
     //Get next character
     if(cin.peek() == '\'')//Encountered end of literal
     {
       tokenSegment.push_back(c);//Add character to word
       break;
     }
     else
       cin.putback(c);//We have made a mistake, put last character back
   }
   if(cin.peek() == '\''){//Encountering delimited '
     if(c == '\\')
       continue;
     //cout << "Next character is '. breaking \n";
     break;
   }
   if(cin.peek() == cin.eof()){//Encountering end of file
     //cout << "Next character is eof. breaking \n";
     break;
   }
 }
 while(true);

 cin.get(c);//Get end quote
 tokenSegment.push_back(c);//Add it to to token

 if(tokenSegment.length() > 4 || tokenSegment[1] != '\\' && tokenSegment.length() > 3)//Checks
                                                                                //length
                                                                                //of
                                                                                //character
                                                                                //literal
 {
   cerr <<"character constant " << tokenSegment << " too long on line " << line << endl;//Print
                                                                                      //error
                                                                                      //message
   tokenSegment = " ";
   return false;
 }

 return true;
 }
  else 
    return false;

}

bool isString(char c)//Check if encountered character is beginning of string
{

  if(c == '"')//The character is a ditto
  {
    //cout << "\n Identified string beginning. \n";
    //cout << "C is: " << (char)c << " Peek is: " << (char)cin.peek() << endl;

    tokenSegment = c;//Add character to current token

    if(cin.peek() == '\n')
    {
      cerr << "missing \" for " << tokenSegment << " on line " << line << endl;//Print error message for unfinished string
//line++;
tokenSegment = " ";//Reset global token contents
return false;
}
else if(cin.peek() == '"')
{
cin.get(c);//Grab final ditto
tokenSegment.push_back(c);//Add to string token
return true;
}


do //Until next ditto grab characters
{

cin.get(c);//Grab next token
tokenSegment.push_back(c);//Add character to new token

//cout << "C is: " << c << " Peek is: " << (char)cin.peek() << endl;

if(cin.peek() == '\n')
{
cerr << "missing \" for " << tokenSegment << " on line " << line << endl;//Print error message for unfinished string
//line++;
tokenSegment = " ";//Reset global token contents
return false;
}
if(cin.peek() == '"' && c != '\\'){//Next character is a ditto and current is a delimiter
//cout << "Next character is \". breaking \n";
break;//End loop
}
else if(cin.peek() == cin.eof()){//About to reach end of file
//cout << "Next character is eof. breaking \n ";
break;//End loop
}
}
while(true);

cin.get(c);//Grab final ditto
tokenSegment.push_back(c);//Add to string token

return true;
}
else 
return false;
}

void SortVector()//Arrange vector by tokCount in descending order
{
bool sorted = false;//Flag indicating if whether the vector has been sorted
while(!sorted)//While not sorted
{
sorted = true;//Indicated sorted until learning otherwise
for(itr = Tokens.begin(); itr != Tokens.end(); itr++)//Move through vector
{
//cout << "Tokens compared: " << itr->contents << ", " << (itr+1)->contents << endl;
if(itr != Tokens.end()-1 && (itr->tokCount < (itr+1)->tokCount || (itr->contents.length() < (itr+1)->contents.length() && itr->tokCount == (itr+1)->tokCount) || (itr->contents > (itr+1)->contents && itr->tokCount == (itr+1)->tokCount && itr->contents.length() == (itr+1)->contents.length())))//Compare counts and lengths of token names
{
sorted = false;//Indicate vector is unsorted
//cout << "Tokens swapped: " << itr->contents << ", " << (itr+1)->contents << endl;
int holder;//Holder int for swap
holder = itr->tokCount;//Swap tokCount
itr->tokCount = (itr+1)->tokCount;
(itr+1)->tokCount = holder;
itr->contents.swap((itr+1)->contents);//Swap contents
}

}
}
}

void IgnoreComment(char c)//Checks if character is beginning of comment
{
	int nlcount = 0;
	string s;
	s = c;//Add char to comment


if(cin.peek() == '/')//There is another /, indicating a line comment
{
	getline(cin,s);//Grab the entire line
	//cout << "Got line comment: " << s;
}
else if(cin.peek() == '*')//There is a * coming up, indicating a multiline comment
{
	//cout << "Got multiline comment: ";
do //Obtain every char until next '*/'
{
	cin.get(c);//Grab next character
	if(c == '\n')
	{
		line++;
		nlcount++;
}
//cout << (char)c;
if(c == '*' && cin.peek() == '/')//Found */
	break;//Exit loop
if(cin.peek() == cin.eof() || nlcount > 10000)//Reached EOF without closing comment.
{
	cerr << "unclosed comment\n";
	return;
}
}
while(true);

 cin.get(c);//Obtain last character

 }
}
