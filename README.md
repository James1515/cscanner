## Cscan, a C++scanner for the C Programming Language

### Description:
cscan is a lexical analyzer for a C compiler. It identifies the C tokens from its standard input and writes them to its standard output, one per line. Afterwards it prints the number of occurrences of each type of token (number, ident, char, string, or the lexeme for the remaining types of tokens) in descending order.

### Compile:
`g++ -g -o cscan ortiz.cpp`

### Show output on seperate txt file:
`./cscan < example.cpp > output.txt`